/**
 * Created by ahadri on 09/12/16.
 */


const winston = require('winston');
winston.emitErrs = true;

const logger = new winston.Logger({
	transports: [
		new winston.transports.Console({
			level: 'debug',
			handleExceptions: true,
			json: false,
			colorize: true,
			prettyPrint: true
		})
	],
	exitOnError: true
});

logger.stream = {
	write: (message) => {
		logger.info(message);
	}
};


module.exports = logger;

