/**
 * Created by ahadri on 09/12/16.
 */

let mongoose = require('mongoose'),
	redis = require('redis'),
	config   = require('../config/db.config');



const dbOptions = { replset : { socketOptions: { connectTimeoutMS : config.dbTimeOut } } , server : { poolSize : config.poolSize}};

const jumiaDB = mongoose.createConnection(config.jumiaDb.url, dbOptions);
const kaymuDB = mongoose.createConnection(config.kaymuDb.url, dbOptions);
const redisClient = redis.createClient(config.REDIS_URL);

let dbFactory = () => {

	function getDbConnection(supplier, cb) {
		if (supplier === 'jumia' && jumiaDB!= null) {
			return cb(null, jumiaDB);
		} else if (supplier === 'kaymu' && kaymuDB!= null) {
			return cb(null, kaymuDB);
		} else {
			return cb(new Error('Error while connecting to the db: check supplier name or connection info!'));
		}
	}

	function getRedisClient() {
		return redisClient;
	}

	return {
		getDbConnection,
		getRedisClient
	};
};


module.exports = dbFactory;
