/**
 * Created by ahadri on 08/12/16.
 */

let jumiaService = require('../service/jumiaService'),
	kaymuService = require('../service/kaymuService');


function getSupplierService(supplier) {
	'use strict';
	if (supplier === 'jumia') {
		return jumiaService();
	}else if(supplier === 'kaymu') {
		return kaymuService();
	}
}


module.exports = getSupplierService;
