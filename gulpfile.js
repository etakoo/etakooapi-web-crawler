/**
 * Created by ahadri on 05/12/16.
 */

let gulp = require('gulp'),
	$ = require('gulp-load-plugins')({lazy: true}),
	spawn = require('child_process').spawn,
	node,
	config = require('./config/gulp.config')();


/**
 * Task to analyse the code using ESLint
 */
gulp.task('lint', () => {
	'use strict';
	log('Analyzing source code with ESlint');

	return gulp
		.src(config.jsFiles)
		.pipe($.eslint())
		// Format the output if there is error!
		.pipe($.eslint.format());
	// skip at the first error with output the status 1
	//	.pipe($.eslint.failAfterError());
});

/**
 * Launch the server task.
 */

gulp.task('server', () => {
	if (node) {
		node.kill();
	}
	node = spawn('node', [config.serverFile], {stdio: 'inherit'});
	node.on('close', (code) => {
		'use strict';
		if (code === 8) {
			log('Error detected, waiting for changes ...');
		}
	});
});


/**
 * Start the reloading task, if a change occurs on js file.
 */
gulp.task('default', () => {
	'use strict';
	gulp.start('server');
	gulp.watch([config.jsFiles], () => {
		gulp.start(['lint' ,'server']);
	});
});


/**
 * If error, kill the node process
 */
process.on('exit', () => {
	'use strict';
	if (node) {
		node.kill();
	}
});

/**
 * Defined function for logging
 * @param msg : the message to log
 */
function log(msg) {
	'use strict';
	if (typeof (msg) === 'object') {
		for (var item in msg) {
			if (msg.hasOwnProperty(item)) {
				$.util.log($.util.colors.blue(msg[item]));
			}
		}
	} else {
		$.util.log($.util.colors.blue(msg));
	}
}
