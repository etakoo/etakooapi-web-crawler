/**
 * Created by ahadri on 25/12/16.
 */

let express               = require('express'),
	userService           = require('../service/userService')(),
	authenticationService = require('../service/authenticationService')(),
	userRouter            = express.Router();


let router = () => {
	'use strict';

	userRouter.route('/login')
		.post( (req, res) => {
			let reqBody = req.body;
			let userInfo = {'email': reqBody.email, 'psswd': reqBody.psswd};
			authenticationService.authUser(userInfo, (err, full_name) => {
				if (err) {
					console.log('--- > ' + err.message);
					return res.status(404).send();
				} else {
					return res.status(200).send({'flname': full_name});
				}
			});
		});

	userRouter.route('/signup')
		.post( (req, res) => {
			let reqBody = req.body;
			let userInfo = {'email': reqBody.email, 'psswd': reqBody.psswd, 'flname': reqBody.flname};
			userService.addUser(userInfo, (err) => {
				if (err) {
					return res.status(401).send();
				} else {
					return res.status(200).send();
				}
			});
		});

	return userRouter;

};


module.exports = router;




