/**
 * Created by ahadri on 30/11/16.
 */

let express           = require('express'),
	productsRouter    = express.Router(),
	async             = require('async'),
	redisClient       = require('../factory/dbFactory')().getRedisClient(),
	productService    = require('../service/productService')(),
	collectionService = require('../service/collectionService')();
let router = () => {
	productsRouter.route('/details')
		.post((req, res) => {
			'use strict';
			let product_info = {'supplier': req.body.supplier, 'url': req.body.url};
			productService.getProductInfo(product_info, (err, product_details) => {
				if (err) {
					return res.status(404).send({'msg': err.message});
				} else {
					return res.status(200).send({'msg': product_details});
				}
			});
		});
	productsRouter.route('/getall')
		.get((req, res) => {
			let response = [];
			async.forEach(['jumia', 'kaymu'], (supplierName, callback) => {
				redisClient.get(supplierName, (err, collections) => {
					if (collections) {
						response.push({[supplierName]: JSON.parse(collections)});
					} else {
						collectionService.getCollectionsProducts(supplierName, (err, collections) => {
							redisClient.set(supplierName, JSON.stringify(collections));
							let supplierProducts = {[supplierName]: collections};
							response.push(supplierProducts);
						});
					}
					return callback();
				});
			},
				(err) => {
					if (err) {
						return res.status(404).send({'msg': err.message});
					} else {
						return res.status(200).send({'msg': response});
					}
				});
		});

	productsRouter.route('/search/:query')
		.get((req, res) => {
			let userQuery = req.param('query');
			productService.searchQuery(userQuery, (err, products) => {
				if (err) {
					return res.status(404).send({'msg': err.message});
				} else {
					return res.status(200).send({'msg': products});
				}
			});
		});
	return productsRouter;
};


module.exports = router;
