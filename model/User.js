/**
 * Created by ahadri on 25/12/16.
 */

/**
 * Created by ahadrii on 29/07/16.
 */
let userDBURI = require('../config/db.config').usersDB.url,
	mongoose = require('mongoose').connect(userDBURI),
	Schema = mongoose.Schema,
	bcrypt = require('bcrypt-nodejs');

mongoose.set('debug', true);
/**
 * Creating the Schema
 */
var userSchema = new Schema({
	full_name: {type: String, required: 'full_name is required'},
	email: {type: String, required: 'email is required'},
	password: {type: String, required: 'password is required',
		min: [8, 'Not a valid password']},
});

/**
 * Defining the methods
 */

// generate a hash for the password before any save operation
userSchema.pre('save', function(next) {
	if (this.isModified('password') || this.isNew) {
		bcrypt.genSalt(10, (err, salt) => {
			if (err) {
				return next(err);
			}
			bcrypt.hash(this.password, salt, null, (err, hash) => {
				if (err) {
					return next(err);
				}
				this.password = hash;
				next();
			});
		});
	} else {
		return next();
	}
});

// checking if password is valid
userSchema.methods.comparePassword =  function(password, cb){
	bcrypt.compare(password, this.password, function (err, isMatch){
		if (err) {
			return cb(err);
		}
		cb(null, isMatch);
	});
};
/**
 * Creating the model using the Schema above
 */

let User = mongoose.model('User', userSchema);

/**
 * Expose the model
 */
module.exports = {
	User
};
