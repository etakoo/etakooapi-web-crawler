/**
 * Created by ahadri on 30/11/16.
 */

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


/**
 * Creating the product Schema
 */
var productSchema = new Schema({
	_id: Schema.Types.ObjectId,
	description: String,
	title: String,
	url: String,
	price: String,
	supplier: String,
	urlImg: [String]
});


/**
 * Creating the model
 */

var Product = mongoose.model('Product', productSchema);


/**
 * Expose the model
 */

module.exports = {
	Product: Product
};
