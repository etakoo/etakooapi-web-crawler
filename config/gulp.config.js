/**
 * Created by ahadri on 05/12/16.
 */

module.exports = () => {
	var config = {
		serverFile : './index.js',
		jsFiles: [
			'./*.js',
			'./route/*.js',
			'./service/*.js',
			'./model/*.js',
			'./config/*.js',
			'./util/*.js',
			'./factory/*.js'
		],
	};
	return config;
};

