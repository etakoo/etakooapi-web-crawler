/**
 * Created by ahadri on 30/11/16.
 */

module.exports = {
	'jumiaDb': {
		'url': 'mongodb://hatim:hatim@ds119788.mlab.com:19788/jumia'
	},
	'kaymuDb': {
		'url': 'mongodb://hatim:hatim@ds119598.mlab.com:19598/kaymu'
	},
	'usersDB': {
	'url': 'mongodb://hatim:hatim@ds054289.mlab.com:54289/users'
	},
	'dbTimeOut': 30,
	'poolSize': 7,
	'REDIS_URL': 'redis://h:p3d281650fd88c1c143ecc5c6916ee27bbabd648cb8f7c4315a4ae951bd98e542@ec2-54-221-253-136.compute-1.amazonaws.com:11859'
};
