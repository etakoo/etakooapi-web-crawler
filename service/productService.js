/**
 * Created by ahadri on 08/12/16.
 */

let async                  = require('async'),
	supplierServiceFactory = require('../factory/supplierFactory'),
	dbFactory              = require('../factory/dbFactory')(),
	redisClient            = require('../factory/dbFactory')().getRedisClient(),
	collectionService      = require('../service/collectionService')(),
	logger = require('../util/logger');

let productService = () => {
	'use strict';

	let getProductInfo = (productsInfo, cb) => {
		let supplier   = productsInfo.supplier,
		    productUrl = productsInfo.url;

		let supplierService = supplierServiceFactory(supplier);
		async.parallel([
			(callback) => {
				supplierService.getDetails(productUrl, (err, productDetails) => {
					if (err) {
						return callback(new Error(err.message));
					} else {
						let details = productDetails;
						return callback(null, details);
					}
				});
			},
			(callback) => {
				supplierService.getImgUrls(productUrl, (err, urls) => {
					if (err) {
						callback(new Error(err.message));
					} else {
						let imgUrls = urls;
						return callback(null, imgUrls);
					}
				});
			}
		], (err, productInfo) => {
			if (err) {
				return cb(new Error(err.message));
			} else {
				let infoJson = {
					'details': productInfo[0],
					'imgs'   : productInfo[1]
				};
				return cb(null, infoJson);
			}
		});
	};

	let searchQuery = (userQuery, cb) => {
		let query = '.*' + userQuery + '.*';
		let searchResult = [];
		let queryInfo;
		async.forEach(['jumia', 'kaymu'], (supplierName, callback) => {
			dbFactory.getDbConnection(supplierName, (err, supplierDbConn) => {
				if (err) {
					cb(new Error(err.message));
				} else {
					redisClient.get('collectionsName', (err, collectionsName) => {
						if (collectionsName) {
							async.forEach(JSON.parse(collectionsName), (collectionName, clbk) => {
								queryInfo = {query, collectionName, supplierDbConn};
								collectionService.searchProducts(queryInfo, (err, products) => {
									if (err) {
										clbk(new Error(err.message));
									} else {
										searchResult = [...searchResult, ...products];
										return clbk();
									}
								});
							},
								(err) => {
									if (err) {
										return callback(new Error(err.message));
									} else {
										return callback();
									}
								});
						} else {
							return callback(new Error('No collections found!'));
						}
					});
				}
			});
		},
			(err) => {
				if (err) {
					cb(new Error(err.message));
				} else {
					cb(null, searchResult);
				}
			});
	};


	return {
		getProductInfo,
		searchQuery
	};

};

module.exports = productService;

