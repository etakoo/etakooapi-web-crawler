/**
 * Created by ahadri on 02/12/16.
 */

let cheerio = require('cheerio'),
	request = require('request'),
	logger = require('../util/logger'),
	util = require('util');


let jumiaService = () => {

	let getDetails = (productUrl, callback) => {
		let requestOptions = {encoding: 'ascii', method: 'get', uri: productUrl};
		request(
			requestOptions, (err, response, html) => {
				if (!err) {
					let $ = cheerio.load(html);
					let selector = $('body > main > section.sku-detail > div.details-wrapper > div.details.-validate-size > div.detail-features > div.list.-features.-compact.-no-float > ul > li');
					let details = Array.from(selector);
					let productsDetails =  details.map((detail) => $(detail).contents()[0].data);
					return callback(null, productsDetails);
				} else {
					return callback(new Error(err.message));
				}
			});
	};

	let getImgUrls = (productUrl, callback) => {
		let requestOptions = {encoding: 'ascii', method: 'get', uri: productUrl};
		request(
			requestOptions, (err, response, html) => {
				if (!err) {
					let $ = cheerio.load(html);
					let urls = []; // where we will keep the urls to send it back to the callback
					let imgsUrl = $('#thumbs-slide a').children();
					Array.from(imgsUrl).forEach((link) => {
						let src = $(link).attr('alt');
						urls.push(src);
					});
					return callback(null, urls);
				} else {
					return callback(new Error(err.message));
				}
			});
	};
	return {
		getDetails,
		getImgUrls
	};
};

module.exports = jumiaService;

