/**
 * Created by ahadri on 08/12/16.
 */

let cheerio = require('cheerio'),
	request = require('request');


let kaymuService = () => {
	'use strict';

	let getDetails = (productUrl, callback) => {
		let requestOptions = {encoding: 'ascii', method: 'get', uri: productUrl};
		request(
			requestOptions, (err, response, html) => {
				if (!err) {
					let $ = cheerio.load(html);
					let selector =  $('#kaymu > div:nth-child(6) > div > div.product-description');
					let productDescSel =  $(selector).contents()[0];
					let productDesc;
					if (productDescSel !== undefined)  {
						productDesc = productDescSel.data == null ? '' : productDescSel.data;
					} else if (productDescSel === undefined){
						return callback(null,['']);
					} else if (!productDesc) {
						productDesc = $(selector).find('p').contents()[0].data;
					}
					return callback(null,[productDesc]);
				} else {
					return callback(new Error(err.message));
				}
			});
	};

	let getImgUrls = (productUrl, callback) => {
		let requestOptions = {encoding: 'ascii', method: 'get', uri: productUrl};
		request(
			requestOptions, (err, response, html) => {
				if (!err) {
					let $ = cheerio.load(html);
					let imgsUrl = Array.from($('#pdp-info > div.pdp-image.small-6.df > div > div.carousel-wrapper.pas > div > div > div > img'));
					let urls = imgsUrl.map( (url) => $(url).attr('data-placeholder-zoom'));
					return callback(null, urls);
				} else {
					return callback(new Error(err.message));
				}
			});
	};

	return {
		getDetails,
		getImgUrls
	};

};

module.exports = kaymuService;

