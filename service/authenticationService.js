/**
 * Created by ahadri on 25/12/16.
 */


let User = require('../model/User').User;


let authenticationService = () => {
	let authUser = (userInfo, cb) => {
		User.findOne({email: userInfo.email}, function (err, user){
			if(err) {
				return cb(new Error(err.message));
			}
			if (!user) {
				return cb(new Error('User Not Found!'));
			}
			user.comparePassword(userInfo.psswd, function (err, result) {
				if (err) {
					return cb(new Error(err.message));
				} else if(result){
					return cb(null, user.full_name);
				}
				return cb(new Error('Password not match'));
			});
		});
	};
	return {
		authUser
	};
};

module.exports = authenticationService;
