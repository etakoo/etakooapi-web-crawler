/**
 * Created by ahadri on 09/12/16.
 */

let async     = require('async'),
	dbFactory = require('../factory/dbFactory')(),
	logger = require('../util/logger');

let collectionService = () => {

	let getCollectionsName = (supplierDbConn, cb) => {
		dbFactory.getRedisClient().get('collectionsName', (err, collectionsName) => {
			'use strict';
			if (collectionsName) {
				return cb(null, JSON.parse(collectionsName));
			} else {
				supplierDbConn.db.listCollections().toArray(function (err, collections) {
					if (err) {
						return cb(err.message);
					}
					else {
						let collectionsName = collections
							.map((collection) => collection.name)
							.filter((collection) => collection.split('.').length === 1 && collection.split('-').length === 1);
						return cb(null, collectionsName);
					}
				});
			}
		});
	};

	let getCollectionsProducts = (supplier, cb) => {
		let dbConnection = null;
		async.waterfall([
			(callback) => {
				dbFactory.getDbConnection(supplier, (err, dbConn) => {
					if (err) {
						return cb(new Error(err.message));
					} else {
						dbConnection = dbConn;
						callback(null, dbConn);
					}
				});
			},
			(dbConn, callback) => {
				getCollectionsName(dbConn, (err, collectionsName) => {
					if (err) {
						return cb(new Error(err.message));
					} else {
						return callback(null, collectionsName);
					}
				});
			}
			,
			(collectionsName) => {
				let collections = [];
				let collection;
				async.forEach(collectionsName, (collectionName, callback) => {
					getCollectionProducts(dbConnection, collectionName, (err, docs) => {
						if (err) {
							return cb(new Error(err.message));
						} else {
							collection = {[collectionName]: docs};
							collections.push(collection);
							callback();
						}
					});
				},
					() => {
						cb(null, collections);
					}
				);
			}
		]);
	};

	let getCollectionProducts = (supplierDbConn, collectionName, cb) => {
		'use strict';
		supplierDbConn.db.collection(collectionName, (err, collection) => {
			collection.find({}, {_id: 0, description: 0}).limit(2).toArray((err, docs) => {
				if (err) {
					return cb(new Error(err.message));
				} else {
					docs = JSON.stringify(docs);
					return cb(null, docs);
				}
			});
		});
	};

	let searchProducts = (queryInfo, cb) => {
		'use strict';
		let query         = queryInfo.query,
		    collectionName = queryInfo.collectionName,
		    supplierDbConn = queryInfo.supplierDbConn;
		logger.info('---------' + query);
		supplierDbConn.db.collection(collectionName, (err, collection) => {
			collection.find({title: {$regex: query, $options: 'i'}}, {_id: 0, description: 0}).limit(5).toArray((err, docs) => {
				if (err) {
					return cb(new Error(err.message));
				} else {
					return cb(null, docs);
				}
			});
		});

	};

	return {
		getCollectionsName,
		getCollectionsProducts,
		searchProducts
	};
};

module.exports = collectionService;
