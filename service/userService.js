/**
 * Created by ahadri on 25/12/16.
 */


let User   = require('../model/User').User;

let userService = () => {

	let addUser = (userInfo, cb) => {

		User.findOne({email: userInfo.email}, (err, user) => {
			'use strict';
			if (err) {
				return cb(new Error(err.message));
			} else if(user) {
				return cb(new Error('Email Already exists'));
			}
			let newUser = new User({
				full_name: userInfo.flname,
				password : userInfo.psswd,
				email    : userInfo.email
			});
			newUser.save((err) => {
				if (err) {
					return cb(new Error(err.message));
				}
				return cb(null, true);
			});
		});
	};

	return {
		addUser
	};

};
module.exports = userService;

