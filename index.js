/**
 * Created by ahadri on 30/11/16.
 */

let express = require('express'),
	bodyParser = require('body-parser'),
	helmet = require('helmet'),
	logger = require('./util/logger');
/**
 * Config
 */

// Application
let port = process.env.PORT || 5000,
	app = express();


// Middleware
app.use(helmet());
app.use(require('morgan')('dev', {'stream' : logger.stream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

/**
 * Routing
 */

// Routes
let productRouter = require('./route/productRouter')();
let userRouter = require('./route/userRouter')();



// Routing
app.use('/products', productRouter);
app.use('/users', userRouter);

/**
 * Lunching the app
 */
app.listen(port, () => {
	'use strict';
	logger.info('Starting the server on : localhost:' + port);
})  ;

